package common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ronal
 */
public class ConfReader {
    
    Properties properties;
    InputStream input;

    public ConfReader() {
        try {
            input = new FileInputStream("credenciales.properties");
            properties = new Properties();
            properties.load(input);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConfReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ConfReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String get(String key) {
        return properties.getProperty(key);
    }
    
}
